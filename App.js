
import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './app/Home.js';
import Details from './app/Details.js';
import SearchView from './app/Search.js';

function DetailsScreen({ navigation }) {
  /* 2. Get the param */
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      {/* <Text>itemId: {JSON.stringify(itemId)}</Text> */}
      {/* <Text>otherParam: {JSON.stringify(otherParam)}</Text> */}
      <Button
        title="Go to Details... again"
        onPress={() =>
          navigation.push('Details', {
            // itemId: Math.floor(Math.random() * 100),
          })
        }
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} options={{
          headerShown: false
        }} />
        <Stack.Screen name="Search" component={SearchView} options={{
          headerShown: false
        }} />
        <Stack.Screen name="Details" component={Details} options={{
          headerShown: false
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;