import React, { Component } from 'react'

import {
    Button,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    StatusBar,
    NativeModules,
    SafeAreaView,
    FlatList,
    ToastAndroid,
} from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, TextInput } from 'react-native-gesture-handler';


const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

const screenWidth = Dimensions.get('window').width;

export default class Details extends Component {
    constructor(props) {
        super(props)
        this.navigation = props.navigation
        this.params = props.route.params

        this.state = {
            planetsData: []
        }

        // this._renderTerrain = this._renderTerrain.bind(this)
    }

    componentDidMount() {
        this.getPlanets()
    }

    getPlanets() {
        return fetch(this.params.url)
            .then((response) => response.json())
            .then((json) => {

                this.setState({
                    planetsData: json,
                })
            })
            .catch((error) => {
                console.error(error);
            });
    };

    _renderPlanets() {
        var loopDetails = (key, value) => {
            return <View style={{
                backgroundColor: "white",
                width: screenWidth - 30,
                padding: 10,
                marginVertical: 5,
                marginHorizontal: 15,
                borderRadius: 5,
                elevation: 2,
                flexDirection: "row",
                alignItems: "flex-end",
                justifyContent: "space-between",
            }}>
                <Text style={{
                    fontSize: 10,
                }}>
                    {key.replace("_", " ")}
                </Text>
                <Text style={{
                    fontSize: 21,
                }}>
                    {value}
                </Text>
            </View>
        }

        var renderDetails = () => {
            return Object.keys(this.state.planetsData).map((key) => {
                if(!["name", "residents", "films", "created", "edited", "url"].includes(key)){
                    return loopDetails(key, this.state.planetsData[key])
                }
            });

            for (var key in this.state.planetsData) {
                return loopDetails(key, this.state.planetsData[key])
            }
        }

        return <View key={`planets${this.state.planetsData}`}>
            <Text style={{
                margin: 20,
                fontSize: 18,
            }}>
                Planet Details
            </Text>
            {renderDetails()}
        </View>
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                <View style={{ flex: 1, }}>
                    <StatusBar translucent={true} backgroundColor={"transparent"} lightContent={false} />
                    <Image style={{
                        width: screenWidth,
                        height: screenWidth * 0.7,
                    }} source={{
                        uri: 'https://scx2.b-cdn.net/gfx/news/2020/earthclimate.jpg'
                    }} />
                    <View style={{ ...styles.headerContainer, position: "absolute", marginTop: STATUSBAR_HEIGHT }}>
                        <TouchableOpacity style={{ ...styles.headerItem, alignItems: "center", }}
                            onPress={() => {
                                this.navigation.goBack()
                            }}>
                            <Image style={{ ...styles.iconStyle, }}
                                source={{
                                    uri: "https://cdn.iconscout.com/icon/free/png-512/back-arrow-1767507-1502574.png"
                                }}
                            />
                        </TouchableOpacity>
                        <View style={{
                            ...styles.headerItem,
                            flex: 1,
                            paddingHorizontal: 10,
                            justifyContent: "center",
                        }}>
                            <Text style={{
                                fontSize: 21,
                                color: "white",
                            }}>
                                {this.state.planetsData.name}
                            </Text>
                        </View>
                        {/* <Image style={{ ...styles.headerItem, }}
                            source={{
                                uri: "https://upload.wikimedia.org/wikipedia/en/b/bf/EVOS_Esports_logo.png"
                            }}
                        /> */}
                        <View style={{ ...styles.headerItem, }}></View>
                    </View>

                    <ScrollView style={styles.container}>
                        {this._renderPlanets()}
                    </ScrollView>
                </View>
            </View>)
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: "row",
        width: screenWidth,
        paddingVertical: 10,
        paddingHorizontal: 10,
        zIndex: 3,
        justifyContent: "space-between",
        backgroundColor: "rgba(0,0,0,0)",
        // elevation: 5,
    },
    headerItem: {
        width: 50,
        height: 50,
    },
    iconStyle: {
        width: 40,
        height: 40,
        marginTop: 3,
        tintColor: "white"
    },
    container: {
        flex: 1,
        // marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    terrainItem: {
        borderWidth: 1,
        borderColor: "lightgrey",
        padding: 5,
        marginHorizontal: 5,
        marginBottom: 5,
    },
    terrainItemText: {
        color: "grey",
        fontSize: 10,
    },
    planetsCardContainer: {
        elevation: 2,
        padding: 10,
        backgroundColor: "white",
        marginVertical: 5,
        marginHorizontal: 10,
        borderRadius: 10,
    }
});