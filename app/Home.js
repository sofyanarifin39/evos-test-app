import React, { Component } from 'react'

import {
    Button,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    StatusBar,
    NativeModules,
    SafeAreaView,
    FlatList,
} from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';

const { StatusBarManager } = NativeModules;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const API_URL = 'https://swapi.dev/api/planets/'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.navigation = props.navigation
        this.params = props.route.params
        this.page = 1
        this.totalPage = 1
        this.isReachedEnd = false

        this.state = {
            planetsData: [],
            keyword: "",
            noData: false,
        }

        this._callAPI = this._callAPI.bind(this)
        this._handleLoadMore = this._handleLoadMore.bind(this)
        this._renderTerrain = this._renderTerrain.bind(this)
        this._renderItem = this._renderItem.bind(this)
        this._handleSearch = this._handleSearch.bind(this)

    }

    componentDidMount() {
        this._callAPI(API_URL + `?page=${this.page}` + '', this.page, 'planetsData')
    }

    _callAPI(url, page, stateName) {
        var url = url + (this.state.keyword.length > 0 ? `&search=${this.state.keyword}` : ``)
        console.log("sopyan", url, this.state.keyword);

        return fetch(url)
            .then((response) => response.json())
            .then((json) => {
                if (json.count) {
                    var total = parseInt(json.count) % 10 == 0 ? parseInt(json.count) / 10 : (parseInt(json.count) / 10) + 1
                    this.totalPage = parseInt(total)
                }

                if (json.results) {
                    if (page > 1) {
                        this.setState({
                            [stateName]: [
                                ...this.state[stateName],
                                ...json.results,
                            ],
                        })
                    } else {
                        if (json.results.length == 0) {
                            this.setState({
                                noData: true,
                            })
                        } else {
                            this.setState({
                                [stateName]: json.results,
                                noData: false,
                            })
                        }
                    }
                } else {
                    this.isReachedEnd = true
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    _handleLoadMore() {
        this.page += 1

        if (!this.page <= this.totalPage) {
            this._callAPI(API_URL +
                `?page=${this.page}` + ''

                , this.page, 'planetsData')
        }
    }

    _renderTerrain(terrain) {
        if (terrain) {
            var terrainArray = terrain.split(", ").map((e, i) => {
                return e
            })
            return terrainArray.map((e, i) => {
                return (
                    <View style={{ ...styles.terrainItem, }}>
                        <Text style={{ ...styles.terrainItemText, }}>{e.charAt(0).toUpperCase() + e.slice(1)}</Text>
                    </View>
                )
            })
        }
    }

    _renderItem({ item, index }) {
        return (
            <View style={{ width: screenWidth, }}>
                <TouchableWithoutFeedback
                    style={{ ...styles.planetsCardContainer, }}
                    onPress={() => {
                        this.navigation.navigate("Details", {
                            url: item.url
                        })
                    }}
                >
                    <View style={{ paddingTop: screenWidth * 0.3, }}>
                        <Text style={{ fontSize: 21, padding: 5, }}>
                            {item.name}
                        </Text>
                        <View style={{ flexDirection: "row" }}>
                            {this._renderTerrain(item.terrain)}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>)
    }

    _handleSearch(searchText) {
        this.page = 1
        this.navigation.goBack()

        this.setState({
            keyword: searchText
        }, () => {
            this._callAPI(API_URL + `?page=${this.page}` + '', this.page, 'planetsData')
        })
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                <StatusBar lightContent={false} />
                <View style={{ ...styles.headerContainer, }}>
                    <View style={{ ...styles.headerItem, }}></View>
                    <Image style={{ ...styles.headerItem, }}
                        source={{
                            uri: "https://upload.wikimedia.org/wikipedia/en/b/bf/EVOS_Esports_logo.png"
                        }}
                    />
                    <TouchableOpacity style={{ ...styles.headerItem, alignItems: "center", }}
                        onPress={() => {
                            this.navigation.navigate("Search", {
                                _handleBack: (searchText) => {
                                    this._handleSearch(searchText)
                                }
                            })
                        }}>
                        <Image style={{ ...styles.iconStyle, }}
                            source={{
                                uri: "https://www.mkpartners.com/images/search.png"
                            }}
                        />
                    </TouchableOpacity>
                </View>
                {this.state.keyword.length > 0 ?
                    <View style={{
                        padding: 10,
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}>
                        <Text>
                            Applied search keyword: "{this.state.keyword}"
                    </Text>
                        <TouchableOpacity onPress={() => {
                            this.page = 1
                            this.setState({ keyword: "" }, () => {
                                this._callAPI(API_URL + `?page=${this.page}` + '', this.page, 'planetsData')
                            })
                            
                        }}>
                            <Text style={{ color: "red", fontWeight: "bold", }}>Clear keyword</Text>
                        </TouchableOpacity>
                    </View> : null
                }
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                    {this.state.noData ?
                        <Text style={{ textAlign: "center", }}>{'No result available.\n Please try another search or clear keyword'}</Text> :
                        <FlatList
                            refreshing={true}
                            data={this.state.planetsData}
                            renderItem={this._renderItem}
                            keyExtractor={item => item.name}
                            onEndReached={this._handleLoadMore}
                        />
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: "row",
        width: screenWidth,
        paddingVertical: 10,
        paddingHorizontal: 10,
        zIndex: 3,
        justifyContent: "space-between",
        backgroundColor: "white",
        elevation: 5,
    },
    headerItem: {
        width: 50,
        height: 50,
    },
    iconStyle: {
        width: 40,
        height: 40,
        marginTop: 3,
        tintColor: "navy"
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    terrainItem: {
        borderWidth: 1,
        borderColor: "lightgrey",
        padding: 5,
        marginHorizontal: 5,
        marginBottom: 5,
    },
    terrainItemText: {
        color: "grey",
        fontSize: 10,
    },
    planetsCardContainer: {
        elevation: 2,
        padding: 10,
        backgroundColor: "white",
        marginVertical: 5,
        marginHorizontal: 10,
        borderRadius: 10,
    }
});