import React, { Component } from 'react'

import {
    Button,
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
    StatusBar,
    NativeModules,
    SafeAreaView,
    FlatList,
    ToastAndroid,
} from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, TextInput } from 'react-native-gesture-handler';

import Search from '../storage/realm/models/SearchModel';
import { createSearch, getAllSearches, getSearchById, updateSearch } from '../storage/realm/actions/SearchAction';

const screenWidth = Dimensions.get('window').width;

export default class SearchView extends Component {
    constructor(props) {
        super(props)
        this.navigation = props.navigation
        this.params = props.route.params

        this.search = new Search()
        this.search.searchId = 1
        this.search.searchName = getSearchById(1).result.searchName
        this.state = {
            searchText: "",
            recentSearch: JSON.parse(getSearchById(1).result.searchName),
        }

    }

    componentDidMount() {

    }

    saveSearch() {
        if (!this.search)
            return;

        getAllSearches().result.length == 0 ? createSearch(this.search) : updateSearch(this.search);
    }

    getsearchData() {
        var data = JSON.parse(getSearchById(1).result.searchName)

        this.setState({
            recentSearch: data
        })
    }

    _renderRecentSearch() {
        var data = this.state.recentSearch

        return data.map((item, index) => {
            return (<TouchableOpacity style={{
                width: "100%",
                backgroundColor: "white",
                padding: 10,
                marginVertical: 5,
                elevation: 2,
            }} onPress={() => {
                this.params._handleBack(item)
            }}>
                <Text>
                    {item}
                </Text>
            </TouchableOpacity>)
        })
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                <View style={{ flex: 1, }}>
                    <StatusBar lightContent={false} />
                    <View style={{ ...styles.headerContainer, }}>
                        <TouchableOpacity style={{ ...styles.headerItem, alignItems: "center", }}
                            onPress={() => {
                                this.navigation.goBack()
                            }}>
                            <Image style={{ ...styles.iconStyle, }}
                                source={{
                                    uri: "https://cdn.iconscout.com/icon/free/png-512/back-arrow-1767507-1502574.png"
                                }}
                            />
                        </TouchableOpacity>
                        <View style={{
                            ...styles.headerItem,
                            flex: 1,
                            paddingHorizontal: 10,
                            justifyContent: "center",
                        }}>
                            <TextInput
                                style={{
                                    height: 50,
                                    borderColor: 'lightgrey',
                                    borderWidth: 1,
                                    paddingHorizontal: 15,
                                    borderRadius: 5,
                                }}
                                placeholder={"Type keyword here ..."}
                                onChangeText={searchText => this.setState({ searchText })}
                                value={this.state.searchText}
                            />
                        </View>
                        {/* <Image style={{ ...styles.headerItem, }}
                            source={{
                                uri: "https://upload.wikimedia.org/wikipedia/en/b/bf/EVOS_Esports_logo.png"
                            }}
                        /> */}
                        <TouchableOpacity style={{ ...styles.headerItem, alignItems: "center", justifyContent: "center" }}
                            onPress={() => {
                                var recentSearch = this.state.recentSearch;
                                recentSearch.push(this.state.searchText)
                                this.setState({
                                    recentSearch
                                }, () => {
                                    this.search.searchName = JSON.stringify(recentSearch)
                                    this.saveSearch()

                                    this.params._handleBack(this.state.searchText)
                                })
                            }}>
                            <Text style={{
                                fontWeight: "bold",
                                color: "navy"
                            }}>
                                Search
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text style={{
                            padding: 15,
                        }}>
                            Recent Searches
                    </Text>
                        {this._renderRecentSearch()}
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: "row",
        width: screenWidth,
        paddingVertical: 10,
        paddingHorizontal: 10,
        zIndex: 3,
        justifyContent: "space-between",
        backgroundColor: "white",
        elevation: 5,
    },
    headerItem: {
        width: 50,
        height: 50,
    },
    iconStyle: {
        width: 40,
        height: 40,
        marginTop: 3,
        tintColor: "navy"
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    terrainItem: {
        borderWidth: 1,
        borderColor: "lightgrey",
        padding: 5,
        marginHorizontal: 5,
        marginBottom: 5,
    },
    terrainItemText: {
        color: "grey",
        fontSize: 10,
    },
    planetsCardContainer: {
        elevation: 2,
        padding: 10,
        backgroundColor: "white",
        marginVertical: 5,
        marginHorizontal: 10,
        borderRadius: 10,
    }
});