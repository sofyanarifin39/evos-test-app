import Realm from 'realm';

import createdSearchAction from './actions/SearchAction';
import SearchModel from './models/SearchModel';
import type { SearchActionInterface } from './actions/SearchAction';

const realmInstance = new Realm({
    schema: [SearchModel],
});

export const getRealmInstance = () => realmInstance;

export const SearchAction: SearchActionInterface = createdSearchAction(realmInstance)