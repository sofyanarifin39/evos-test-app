export default class Search {
    searchId: number;
    searchName: string;

    constructor(searchId = 1, searchName = '') {
        this.searchId = searchId;
        this.searchName = searchName;
    }

    getRealmObject() {
        return {
            searchId: this.searchId,
            searchName: this.searchName
        };
    }

    updateObjectInfo(search: any) {
        if (!search)
            return;

        search['searchName'] = this.searchName;
    }

    clone() {
        return new Search(this.searchId, this.searchName);
    }
}

const SearchSchema = {
    name: 'Search',
    properties: {
        searchId: 'int',
        searchName: 'string'
    }
};

Search.schema = SearchSchema;