import Search from '../models/SearchModel';
import Message from '../models/Message';

let Realm = require('realm');
let realm = new Realm({ path: 'search-db.realm', schema: [Search.schema] });

// result: boolean
export const createSearch = (search: Search) => {
    let msg = new Message();
    if (!search) {
        msg.result = false;
        msg.message = 'Invalid search input!';
        return msg;
    }

    // check if search already existed?
    search.searchId = 1;//generateId();
    if (checkIfSearchExists(search.searchId)) {
        msg.result = false;
        msg.message = `Search with id=${search.searchId} existed!`;
        return msg;
    }

    try {
        realm.write(() => {
            realm.create('Search', search.getRealmObject());
        });
        msg.result = true;
        msg.message = 'Create new search successful!';
    } catch(e) {
        msg.result = false;
        msg.message = `${e.message}`;
    } finally {
        return msg;
    }
}

const generateId = () => {
    let searches = getAllSearches().result;
    if (searches.length == 0)
        return 1;

    let sortedSearches = searches.sorted('searchId', true); // sort by searchId descending;
    let firstSearch = sortedSearches[0];
    return firstSearch['searchId'] + 1;
}

// result: realm objects
export const getAllSearches = () => {
    let msg = new Message();
    try {
        msg.result = realm.objects('Search');
        msg.message = 'Get all searches successful!';
    } catch(e) {
        msg.result = [];
        msg.message = 'Get all searches failed!';
    } finally {
        return msg;
    }
}

// result: realm object
export const getSearchById = (id: number) => {
    let msg = new Message();
    let searches = getAllSearches().result;
    let findSearch = searches.filtered(`searchId=${id}`); // return collections
    if (findSearch.length == 0) {
        msg.result = null;
        msg.message = `Not found search with id=${id}`;
    } else {
        msg.result = findSearch[0];
        msg.message = `Found 1 search with id=${id}`;
    }

    return msg;
}

const checkIfSearchExists = (id: number) => {
    let search = getSearchById(id).result;
    return search != null;
}

export const updateSearch = (search: Search) => {
    let msg = new Message();
    if (!search) {
        msg.result = false;
        msg.message = 'Invalid search input!';
        return msg;
    }

    let findSearch = getSearchById(search.searchId).result;
    if (!findSearch) {
        msg.result = false;
        msg.message = `Not found search with id=${search.searchId}`;
        return msg;
    }

    try {
        realm.write(() => {
            search.updateObjectInfo(findSearch);
        });
        msg.result = true;
        msg.message = `Update search with id=${search.searchId} successful`;
    } catch(e) {
        msg.result = false;
        msg.message = `Update search with id=${search.searchId} failed: ${e.message}`;
    } finally {
        return msg;
    }
}

export const deleteSearch = (search: Search) => {
    let msg = new Message();
    if (!search) {
        msg.result = false;
        msg.message = 'Invalid search input!';
        return msg;
    }

    let findSearch = getSearchById(search.searchId).result;
    if (!findSearch) {
        msg.result = false;
        msg.message = `Not found search with id=${search.searchId}`;
        return msg;
    }

    try {
        realm.write(() => {
            realm.delete(findSearch);
        });
        msg.result = true;
        msg.message = `Delete search with id=${search.searchId} successful`;
    } catch(e) {
        msg.result = false;
        msg.message = `Delete search with id=${search.searchId} failed: ${e.message}`;
    } finally {
        return msg;
    }
}